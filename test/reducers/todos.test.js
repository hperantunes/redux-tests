import { expect } from 'chai';
import * as types from '../../src/constants/ActionTypes'
import todos from '../../src/reducers/todos';


describe('Todos reducer', () => {
  it('should return the initial state in the default case', () => {
    expect(todos(undefined, {})).to.eql([
      {
        text: 'Use Redux',
        completed: false,
        id: 0
      }
    ]);
  });

  describe('when handling ADD_TODO', () => {
    describe('and the store is empty', () => {
      it('should add one action to the store', () => {
        expect(
          todos([], {
            type: types.ADD_TODO,
            text: 'Run the tests'
          })
        ).to.eql([
          {
            text: 'Run the tests',
            completed: false,
            id: 0
          }
        ]);
      });
    });
    describe('and the store is not empty', () => {
      it('should add one action to the store', () => {
        expect(
          todos([
            {
              text: 'Use Redux',
              completed: false,
              id: 0
            }
          ], {
              type: types.ADD_TODO,
              text: 'Run the tests'
            })
        ).to.eql([
          {
            text: 'Use Redux',
            completed: false,
            id: 0
          },
          {
            text: 'Run the tests',
            completed: false,
            id: 1
          }
        ]);
      });
    });
  });

  describe('when handling DELETE_TODO', () => {
    it('should delete by id', () => {
      expect(
        todos([
          {
            text: 'Use Redux',
            completed: false,
            id: 0
          },
          {
            text: 'Run the tests',
            completed: false,
            id: 1
          }
        ], {
            type: types.DELETE_TODO,
            id: 1
          })
      ).to.eql([
        {
          text: 'Use Redux',
          completed: false,
          id: 0
        }
      ])
    })
  });

});